package com.zhaoj.savemoney.utils;

import com.zhaoj.savemoney.ApplicationData;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferencesUtils {
	public void InitAppDataFromPref(Context context) {
		SharedPreferences preferences = 
				PreferenceManager.getDefaultSharedPreferences (context);
		ApplicationData.bVoiceRecog = preferences.getBoolean("voice_recog", false);
		ApplicationData.accountID = preferences.getInt("account_id", 0);
	}
}
