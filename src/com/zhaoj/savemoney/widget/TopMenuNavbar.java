package com.zhaoj.savemoney.widget;  
  
import com.zhaoj.savemoney.R;

import android.content.Context;  
import android.util.AttributeSet;  
import android.view.LayoutInflater;  
import android.view.View;  
import android.widget.FrameLayout;  
import android.widget.ImageButton;
import android.widget.LinearLayout;  
import android.widget.TextView;

/** 
 * 功能描述：自定义顶部菜单栏 
 * @author android_ls 
 */  
public class TopMenuNavbar extends FrameLayout {  
   
    private LinearLayout llShowMenu;  
    
    
    public TextView tvTitle;
    public ImageButton leftButton;
    public ImageButton rightButton;
  
    /** 
     * 打开左侧菜单的组件的事件监听器 
     */  
    private OnMenuClickListener mOnClickListener;  
  
    public TopMenuNavbar(Context context) {  
        super(context);  
        setupViews();  
    }  
  
    public TopMenuNavbar(Context context, AttributeSet attrs) {  
        super(context, attrs);  
        setupViews();  
    }  
  
    public void setOnClickListener(OnMenuClickListener onClickListener) {  
        mOnClickListener = onClickListener;  
    }  
  
    private void setupViews() { 
    	
        final LayoutInflater mLayoutInflater = LayoutInflater.from(getContext());  
        LinearLayout rlTopNavbar = (LinearLayout) mLayoutInflater.inflate(R.layout.view_topmenu, null);  
        addView(rlTopNavbar);  
  
        llShowMenu = (LinearLayout) rlTopNavbar.findViewById(R.id.topmenulayout);  
  
        tvTitle = (TextView) rlTopNavbar.findViewById(R.id.tv_title);  
        rightButton = (ImageButton)rlTopNavbar.findViewById(R.id.right_button);
        leftButton = (ImageButton)rlTopNavbar.findViewById(R.id.left_button);
        
        llShowMenu.setOnClickListener(new View.OnClickListener() {  
  
            @Override  
            public void onClick(View v) {  
                if (mOnClickListener != null) {  
                    mOnClickListener.onClick();  
                }  
            }  
        });  
    }  
    
    public interface OnMenuClickListener {  
        /** 
         * 当前选中的Item事件处理器 
         * @param groupPosition 所属组Id 
         * @param childPosition 在所属组内的位置 
         */  
        public abstract void onClick();  
    }
} 