package com.zhaoj.savemoney.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.InvalidInputException;
import com.zhaoj.savemoney.R;
import com.zhaoj.savemoney.Task.InstallVRTask;
import com.zhaoj.savemoney.ui.AddNewRecordActivity.GalleryAdapter.ViewHolder;
import com.zhaoj.savemoney.utils.ConstantsUtils;
import com.zhaoj.savemoney.widget.DateMenuBar;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.speech.RecognizerIntent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

@SuppressWarnings("deprecation")
public class AddNewRecordActivity extends Activity {
	private static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;
	private static final String DATEFORMAT_STRING = "yyyy-MM-dd";

	private static final String QUERY_COSTTYPE_SQL_STRING = "select * from costtype_tbl where costtype_kind = ?";
	private static final String INVALID_INPUT_MSG = "请输入金额！";
	private BaseAdapter outAdapter;
	private BaseAdapter inAdapter;

	private Gallery gallery;
	private DateMenuBar mDateMenuBar;
	private EditText remarkEditText;
	private EditText amountEditText;
	private ToggleButton outButton;
	private ToggleButton inButton;

	@SuppressLint({ "NewApi", "ShowToast" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_add_new_record);

		gallery = (Gallery) findViewById(R.id.gallery1);
		InitGalleryAdapter();
		gallery.setAdapter(outAdapter);
		gallery.setSelection(gallery.getCount() / 2);

		mDateMenuBar = (DateMenuBar) findViewById(R.id.date_Menu_Bar);
		mDateMenuBar.llDateBar.setBackground(getResources().getDrawable(
				R.drawable.maincolor));
		mDateMenuBar.setDateFormatString(DATEFORMAT_STRING);
		mDateMenuBar.leftButton.setImageResource(R.drawable.icon_back);
		mDateMenuBar.rightButton.setImageResource(R.drawable.icon_ok);
		mDateMenuBar.leftButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});
		mDateMenuBar.rightButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					ApplicationData.accountDBHelper.getWritableDatabase()
							.insert("default_account_tbl", null,
									GetInsertContentValues());
					setResult(RESULT_OK);
					finish();
				} catch (InvalidInputException e) {
					Toast.makeText(AddNewRecordActivity.this, e.getMessage(), 1)
							.show();
					e.printStackTrace();
				}
			}
		});
		mDateMenuBar.invalidate();

		outButton = (ToggleButton) findViewById(R.id.toggleButtonOut);
		inButton = (ToggleButton) findViewById(R.id.toggleButtonin);
		outButton.setChecked(true);
		outButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					gallery.setAdapter(outAdapter);
					gallery.setSelection(gallery.getCount() / 2);
					inButton.setChecked(false);
					buttonView.setBackground(getResources().getDrawable(
							R.drawable.button_out_check));
				} else {
					inButton.setChecked(true);
					buttonView.setBackground(getResources().getDrawable(
							R.drawable.button_in_out_uncheck));
				}

			}
		});

		inButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					gallery.setAdapter(inAdapter);
					gallery.setSelection(gallery.getCount() / 2);
					outButton.setChecked(false);
					buttonView.setBackground(getResources().getDrawable(
							R.drawable.button_in_check));
				} else {
					outButton.setChecked(true);
					buttonView.setBackground(getResources().getDrawable(
							R.drawable.button_in_out_uncheck));
				}

			}
		});

		amountEditText = (EditText) findViewById(R.id.amount);
		remarkEditText = (EditText) findViewById(R.id.remarkeditText);
		if (ApplicationData.bVoiceRecog) {

			remarkEditText.setHint(getResources().getString(
					R.string.pref_summary_voice_switch_on));
			remarkEditText.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					try {
						// 通过Intent传递语音识别的模式，开启语音
						Intent intent = new Intent(
								RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
						// 语言模式和自由模式的语音识别
						intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
								RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
						// 提示语音开始
						intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "开始语音");
						// 开始语音识别
						startActivityForResult(intent,
								VOICE_RECOGNITION_REQUEST_CODE);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
						new InstallVRTask(AddNewRecordActivity.this)
								.start(ConstantsUtils.VR_DOWNLOAD_URL_STRING);
					}
				}

			});
		} else {
			remarkEditText.setHint("");
		}
	}

	private ContentValues GetInsertContentValues() throws InvalidInputException {
		ContentValues values = new ContentValues();
		values.clear();

		values.put("defacu_kind", GetCurKind());
		try {
			values.put("defacu_amount", GetCurAmount());
		} catch (Exception e) {
			throw new InvalidInputException(INVALID_INPUT_MSG);
		}

		values.put("defacu_remark", GetCurRemark());
		values.put("defacu_costtype", GetCurCostType());
		values.put("defacu_time", GetCurTime());
		return values;
	}

	@SuppressLint("SimpleDateFormat")
	private String GetCurTime() {
		String dateFormatString;
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		dateFormat.applyPattern(DATEFORMAT_STRING);
		dateFormatString = dateFormat
				.format(mDateMenuBar.curCalendar.getTime());
		return dateFormatString;
	}

	private int GetCurCostType() {
		ViewHolder viewHolder = (ViewHolder) gallery.getSelectedView().getTag();
		return viewHolder.costTypeID;
	}

	private int GetCurKind() {
		if (outButton.isChecked()) {
			return 0;
		} else {
			return 1;
		}
	}

	private Double GetCurAmount() throws InvalidInputException {
		try {
			if (GetCurKind() == 0) {
				return -Double.parseDouble(amountEditText.getText().toString());
			} else {
				return Double.parseDouble(amountEditText.getText().toString());
			}
		} catch (Exception e) {
			throw new InvalidInputException(INVALID_INPUT_MSG);
		}
	}

	private String GetCurRemark() {
		return remarkEditText.getText().toString();
	}

	private void InitGalleryAdapter() {
		outAdapter = new GalleryAdapter(ConstantsUtils.COST_KIND_OUT);
		inAdapter = new GalleryAdapter(ConstantsUtils.COST_KIND_IN);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_new_record, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 回调获取从谷歌得到的数据
		if (requestCode == VOICE_RECOGNITION_REQUEST_CODE
				&& resultCode == RESULT_OK) {
			// 取得语音的字符
			ArrayList<String> results = data
					.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			remarkEditText.setText(results.get(0));
		}
		super.onActivityResult(requestCode, resultCode, data);

	}

	public class GalleryAdapter extends BaseAdapter {
		private int CostTypeICONIDs[];
		private int CostTypeIDs[];
		private String CostTypeNames[];
		int i = 0;

		@SuppressLint("Recycle")
		public GalleryAdapter(int costKind) {
			Cursor cursor = ApplicationData.accountDBHelper
					.getReadableDatabase().rawQuery(QUERY_COSTTYPE_SQL_STRING,
							new String[] { Integer.toString(costKind) });

			CostTypeICONIDs = new int[cursor.getCount()];
			CostTypeIDs = new int[cursor.getCount()];
			CostTypeNames = new String[cursor.getCount()];
			while (cursor.moveToNext()) {
				CostTypeIDs[i] = cursor.getInt(0);
				CostTypeNames[i] = cursor.getString(1);
				CostTypeICONIDs[i] = cursor.getInt(2);
				i++;
			}
		}

		@Override
		public int getCount() {
			return CostTypeIDs.length;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			if (convertView == null) {
				holder = new ViewHolder();
				convertView = View.inflate(AddNewRecordActivity.this,
						R.layout.view_costtypeitem, null);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.imgview_typeitemicon);
				holder.title = (TextView) convertView
						.findViewById(R.id.txtview_typeitemtitle);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			holder.icon.setImageDrawable(getResources().getDrawable(
					CostTypeICONIDs[position]));
			holder.title.setText(CostTypeNames[position]);
			holder.costTypeID = CostTypeIDs[position];
			return convertView;
		}

		class ViewHolder {
			public int costTypeID;
			private ImageView icon;
			private TextView title;
		}
	}

}
