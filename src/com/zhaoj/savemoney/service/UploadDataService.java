package com.zhaoj.savemoney.service;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.Task.RequestBase;
import com.zhaoj.savemoney.Task.UploadTask;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class UploadDataService extends IntentService {

	private static final String TAG = "UploadDataService";
	private static final String qUERY_SHARE_ACCOUNT_SQL_STRING = "select * from account_tbl "
			+ "where account_isshare = ?";

	public UploadDataService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public UploadDataService() {
		super(TAG);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Cursor cursor = ApplicationData.accountManagerDBHelper
				.getReadableDatabase().rawQuery(qUERY_SHARE_ACCOUNT_SQL_STRING,
						new String[] { String.valueOf(1) });
		while (cursor.moveToNext()) {
			RequestBase request = new RequestBase();
			request.setApi(ApplicationData.mApi);
			request.setFileUri(Uri.parse(cursor.getString(cursor
					.getColumnIndex("account_path"))));
			Log.v(TAG,
					"ready to start task:"
							+ cursor.getString(cursor
									.getColumnIndex("account_path")));
			new UploadTask(ApplicationData.settingActivity).start(request);
		}
	}
}
