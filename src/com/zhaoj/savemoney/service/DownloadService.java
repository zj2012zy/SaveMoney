package com.zhaoj.savemoney.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.Task.DownloadTask;
import com.zhaoj.savemoney.Task.RequestBase;
import com.zhaoj.savemoney.utils.ConstantsUtils;

import cn.kuaipan.android.sdk.exception.KscException;
import cn.kuaipan.android.sdk.exception.KscRuntimeException;
import cn.kuaipan.android.sdk.model.KuaipanFile;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

public class DownloadService extends IntentService {
	private static final String TAG = "DownloadDataService";
	private static final String QUERY_ACCOUNT_SQL_STRING = "select * from account_tbl where account_name = ?";
	private ArrayList<KuaipanFile> fileList;

	public DownloadService(String name) {
		super(name);
		fileList = new ArrayList<KuaipanFile>();
		// TODO Auto-generated constructor stub
	}

	public DownloadService() {
		super(TAG);
		fileList = new ArrayList<KuaipanFile>();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if ((ApplicationData.mApi == null)
				|| (!ApplicationData.mApi.isAuthorized())) {
			return;
		}

		try {
			getAllFiles(ApplicationData.mApi
					.metadata(ConstantsUtils.KUPAIN_PATH));
		} catch (KscRuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KscException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (KuaipanFile file : fileList) {
			String remotePath = file.path;
			RequestBase request = new RequestBase();
			request.setApi(ApplicationData.mApi);
			request.setRemotePath(remotePath);
			new DownloadTask(ApplicationData.mainActivity).start(request);
			Log.v(TAG, "getDataFile:" + remotePath);
			// 将下载到的数据库文件加入账本数据库内
			String fileNameString = new File(remotePath).getName().replaceAll("[.][^.]+$", "");
			String fileNamePathString = ConstantsUtils
					.getDBFileName(fileNameString);
			upDateAccountManager(fileNameString, fileNamePathString);
		}
	}

	private void upDateAccountManager(String fileNameString,
			String fileNamePathString) {
		Cursor cursor = ApplicationData.accountManagerDBHelper
				.getReadableDatabase().rawQuery(QUERY_ACCOUNT_SQL_STRING,
						new String[] { fileNameString });
		ContentValues values = new ContentValues();
		values.clear();
		values.put("account_name", fileNameString);
		values.put("account_path", fileNamePathString);
		values.put("account_isshare", 1);
		if (cursor.getCount() != 0) {
			ApplicationData.accountManagerDBHelper.getWritableDatabase()
					.update("account_tbl", values, "account_name = ?",
							new String[] { fileNameString });
		} else {
			ApplicationData.accountManagerDBHelper.getWritableDatabase()
					.insert("account_tbl", null, values);
		}
	}

	public void getAllFiles(KuaipanFile file) {
		if (file.isDirectory()) {
			List<KuaipanFile> childrens = file.getChildren();
			if (childrens != null) {
				int size = childrens.size();
				for (int i = 0; i < size; i++) {
					KuaipanFile children = childrens.get(i);
					if (!children.isDirectory()) {
						fileList.add(children);
					}
				}
			}
		}
	}

}
