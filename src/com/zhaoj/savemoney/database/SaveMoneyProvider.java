/**   
 * SaveMoney
 *
 * @Date     2014-9-30
 * @author   zhaojie 
 *
 **/
package com.zhaoj.savemoney.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class SaveMoneyProvider extends ContentProvider {

	public static final int URI_COSTTYPE = 1;
	public static final int URI_COSTTYPES = 2;
	public static final int URI_ACOUNT = 3;
	public static final int URI_ACOUNTS = 4;
	public static final int URI_ACCOUNTMANAGER = 5;
	public static final int URI_ACCOUNTMANAGERS = 6;

	public static final UriMatcher URI_MATCHER = new UriMatcher(
			UriMatcher.NO_MATCH);
	static {
		URI_MATCHER.addURI(SaveMoneyData.AUTHORITY, "costType", URI_COSTTYPES);
		URI_MATCHER.addURI(SaveMoneyData.AUTHORITY, "costType/#", URI_COSTTYPE);
		URI_MATCHER.addURI(SaveMoneyData.AUTHORITY, "account", URI_ACOUNTS);
		URI_MATCHER.addURI(SaveMoneyData.AUTHORITY, "account/#", URI_ACOUNT);
		URI_MATCHER.addURI(SaveMoneyData.AUTHORITY, "accountManager",
				URI_ACCOUNTMANAGERS);
		URI_MATCHER.addURI(SaveMoneyData.AUTHORITY, "accountManager/#",
				URI_ACCOUNTMANAGER);
	}

	private AccountDBHelper mAccountDBHelper;
	private AccountManagerDBHelper mAccountManagerDBHelper;
	private int curAccountID = -1;

	@Override
	public boolean onCreate() {
		mAccountManagerDBHelper = new AccountManagerDBHelper(getContext());
		checkAccount();
		return false;
	}

	private void checkAccount() {
		SQLiteDatabase db = mAccountManagerDBHelper.getReadableDatabase();
		Cursor cursor = db
				.query(SaveMoneyData.AccountManagerColumns.TABLE_NAME,
						new String[] { SaveMoneyData.AccountManagerColumns.ACOUNT_MANAGER_ID },
						SaveMoneyData.AccountManagerColumns.IS_SELECTED + "=?",
						new String[] { String
								.valueOf(SaveMoneyData.BOOLEAN_TRUE) }, null,
						null, null);

		if ((curAccountID == -1)
				|| (cursor.moveToNext() && cursor
						.getInt(cursor
								.getColumnIndex(SaveMoneyData.AccountManagerColumns.ACOUNT_MANAGER_ID)) != curAccountID)) {
			mAccountDBHelper = new AccountDBHelper(
					getContext(),
					cursor.getString(cursor
							.getColumnIndex(SaveMoneyData.AccountManagerColumns.URI)),
					null,
					cursor.getInt(cursor
							.getColumnIndex(SaveMoneyData.AccountManagerColumns.VERSION_CODE)));
		}
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		int matchCode = URI_MATCHER.match(uri);
		switch (matchCode) {
		case URI_COSTTYPE:
			queryBuilder.setTables(SaveMoneyData.CostTypeColumns.TABLE_NAME);
			break;
		case URI_COSTTYPES:
			queryBuilder.setTables(SaveMoneyData.CostTypeColumns.TABLE_NAME);
		case URI_ACOUNT:
			queryBuilder.setTables(SaveMoneyData.AcountColumns.TABLE_NAME);
		case URI_ACOUNTS:
			queryBuilder.setTables(SaveMoneyData.AcountColumns.TABLE_NAME);
		case URI_ACCOUNTMANAGER:
			queryBuilder
					.setTables(SaveMoneyData.AccountManagerColumns.TABLE_NAME);
		case URI_ACCOUNTMANAGERS:
			queryBuilder
					.setTables(SaveMoneyData.AccountManagerColumns.TABLE_NAME);
		}
		return null;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
