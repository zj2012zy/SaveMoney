/*
 * Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.zhaoj.savemoney.Task;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.zhaoj.savemoney.R;
import com.zhaoj.savemoney.utils.ConstantsUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class InstallVRTask {

	private static final String TAG = "InstallVRTask";

	private ProgressDialog dialog;
	private Activity activity;

	public InstallVRTask(Activity activity) {
		this.activity = activity;
	}

	public void showProgressDialog() {
		dialog = new ProgressDialog(activity);
		dialog.setMessage(activity
				.getString(com.zhaoj.savemoney.R.string.InstellVR));
		dialog.setCancelable(false);
		dialog.show();
	}

	public void hideProgressDialog() {
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
	}

	protected void displayAlert(String title, String message) {

		AlertDialog.Builder confirm = new AlertDialog.Builder(activity);
		confirm.setTitle(title);
		confirm.setMessage(message);

		confirm.setNegativeButton(activity.getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

						dialog.dismiss();
					}
				});

		confirm.show().show();
	}

	protected void displayErrorAlert(String title, String message) {

		AlertDialog.Builder confirm = new AlertDialog.Builder(activity);
		confirm.setTitle(title);
		confirm.setMessage(message);

		confirm.setNegativeButton(activity.getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						activity.finish();
					}
				});

		confirm.show().show();
	}

	public void start(String url) {
		new InstallVR().execute(url);
	}

	private class InstallVR extends AsyncTask<String, Void, String> {

		protected void onPreExecute() {
			Log.v(TAG, "====InstallVR task onPreExecute====");
			showProgressDialog();
		}

		@SuppressLint("SdCardPath")
		protected String doInBackground(String... params) {
			String str = "VR.apk"; // APK的名字
			String fileName = ConstantsUtils.DATABASE_PATH + str; // 我们上面说到路径

			Log.v(TAG, "====InstallVR task doInBackground====");
			if (params == null || params.length != 1) {
				return null;
			}

			try {
				URL url = new URL(params[0]);
				HttpURLConnection connection = (HttpURLConnection) url
						.openConnection();
				connection.setConnectTimeout(10 * 1000); // 超时时间
				connection.connect(); // 连接
				if (connection.getResponseCode() == 200) { // 返回的响应码200,是成功.
					File file = new File(fileName); // 这里我是手写了。建议大家用自带的类
					file.createNewFile();
					InputStream inputStream = connection.getInputStream();
					ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream(); // 缓存
					byte[] buffer = new byte[1024 * 10];
					while (true) {
						int len = inputStream.read(buffer);
						if (len == -1) {
							break; // 读取完
						}
						arrayOutputStream.write(buffer, 0, len); // 写入
					}
					arrayOutputStream.close();
					inputStream.close();

					byte[] data = arrayOutputStream.toByteArray();
					FileOutputStream fileOutputStream = new FileOutputStream(
							file);
					fileOutputStream.write(data); // 记得关闭输入流
					fileOutputStream.close();
				}

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(fileName)),
					"application/vnd.android.package-archive");
			activity.startActivity(intent);
			return fileName;
		}

		protected void onPostExecute(String result) {

			hideProgressDialog();
			if (result != null) {
				Toast.makeText(activity, "语音识别组件已安装完成", Toast.LENGTH_SHORT)
						.show();
			} else {
				Toast.makeText(activity, "语音识别组件安装失败！请手动下载！",
						Toast.LENGTH_SHORT).show();
			}

		}
	}
}
